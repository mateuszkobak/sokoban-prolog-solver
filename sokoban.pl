solve_problem(sokoban, State, _, []) :-
    final_state(sokoban, State)
.

solve_problem(sokoban, State, History, Moves):-
    movement(State, push(Box, Dir), SubMoves),
    update(State, push(Box, Dir), state(NewSoko, NewBoxes)),
    \+ member(state(NewSoko, NewBoxes), History),
    append(SubMoves, [move(_, Dir)|NewMoves], Moves),
    solve_problem(sokoban, state(NewSoko, NewBoxes), [state(NewSoko, NewBoxes) | History], NewMoves)
.    
/***************************************************************************/
/* Your code goes here                                                     */
/* You can use the code below as a hint.                                   */
/***************************************************************************/

solve(Problem, Solution):-

Problem = [Tops, Rights, Boxes, Solutions, sokoban(Sokoban)],
abolish_all_tables,
retractall(top(_,_)),
findall(_, ( member(P, Tops), assert(P) ), _),
retractall(right(_,_)),
findall(_, ( member(P, Rights), assert(P) ), _),
retractall(solution(_)),
findall(_, ( member(P, Solutions), assert(P) ), _),

retractall(initial_state(_,_)),
findall(Box, member(box(Box), Boxes), BoxLocs),
assert(initial_state(sokoban, state(Sokoban, BoxLocs))),
solve_problem(sokoban, state(Sokoban, BoxLocs), [state(Sokoban, BoxLocs)], Solution).

:-include(game)
.